package com.example.recyclerveiwexample;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    List<Worker> workerList = new ArrayList<>();
    Context context;

    public MyAdapter(List<Worker> workerList, Context context) {
        this.workerList = workerList;
        this.context = context;
    }

    public void dataSetChanged(List<Worker> workerList) {
        this.workerList = workerList;
        notifyDataSetChanged();
    }

    public void remove(int position) {
        workerList.remove(position);
        notifyItemRemoved(position);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
//        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_main, viewGroup, false);
        View view;
        if (viewType == 1) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_main_grid_small, viewGroup, false);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_main_grid, viewGroup, false);
        }
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        myViewHolder.name.setText(workerList.get(position).getName());
        myViewHolder.age.setText("Age: " + workerList.get(position).getAge());
        myViewHolder.position.setText("Position: " + workerList.get(position).getPosition());
        Glide.with(context).load(workerList.get(position).getPhoto())
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(myViewHolder.photo);
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {
            return 1;
        } else {
            return 2;
        }
    }

    @Override
    public int getItemCount() {
        return workerList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView age;
        TextView position;
        CircleImageView photo;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            age = itemView.findViewById(R.id.age);
            position = itemView.findViewById(R.id.position);
            photo = itemView.findViewById(R.id.photo);
        }
    }
}
